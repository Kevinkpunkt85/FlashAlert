## FlashAlert ##

Cette application permet d'avoir une alerte visuelle lors de la réception d'un appel, un message SMS/MMS ou lors de la réception d'une notification.

Cette alerte est réalisée grâce au flash de l'appareil photo, si le terminal en est équipé.

Alerte proposée sur les événements  :
 * Alerte sur les appels
 * Alerte sur les messages SMS / MMS
 * Alerte sur les notifications applicatives ( Whatsapp, Banque .... )

Alerte activée sur des conditions :

 * Sur l'ensemble des appels / SMS ou uniquement sur les contacts du terminal.
 * Si l'écran est éteint
 * Si le terminal est à plat.
 * Peut passer outre le mode ne pas déranger.

Paramétrage possible:

 * Mode jour / nuit.

 -------------------------
 Détails des possibilités :

  * Les événements déclenchant l'alerte visuelle sont la réception d'un appel et/ou
  la réception d'un SMS/MMS et/ou la notification d'une application ( Whatsapp, Telegram...)

  * Il est possible d'être notifié sur l'ensemble des appels et/ou SMS/MMS ou uniquement
  si le destinataire se trouve dans vos contacts. Cette option se règle avec les sélecteurs
  "Tous contacts" et "Uniquement mes contacts"

  * Le sélecteur "Uniquement si l'écran est éteint" permet de n'avoir une alerte visuelle
  que si vous n'utilisez pas votre terminal, et que ce dernier a a effectivement l'écran éteint.

  * "Outrepasser le mode ne pas déranger" lorsque ce mode est activé aucune sonnerie ou
  notification n'est émise depuis le terminal ( exemple mode ne pas déranger la nuit ). Il est cependant possible d'ignorer ce mode pour être notifier.

  * "Seulement si le téléphone est à plat" l'application va détecter l'inclinaison du terminal, si celui ci n'est pas à plat, l'alerte visuelle ne sera pas déclenchée.
  L'exemple le plus simple, il est inutile d'avoir l'alerte si le téléphone se trouve dans votre poche.

  * "Utiliser le mode nuit" force l'application à se mettre avec des couleurs plus sombres.
  Si votre terminal utilise déjà un thème sombre se dernier sera automatiquement utilisé.

-------------------------

Permissions utilisées :
  * android.permission.CAMERA = Permet d'accéder au flash de la camera
  * android.permission.FLASHLIGHT = Permet d'accéder au flash de la camera
  * android.permission.RECEIVE_SMS = Permet de détecter la réception d'un SMS
  * android.permission.RECEIVE_MMS = Permet de détecter la réception d'un MMS
  * android.permission.READ_PHONE_STATE =  = Permet de détecter la réception d'un appels
  * android.permission.READ_CONTACTS  = Permet de vérifier si le contact appelant ( ou émetteur d'un message) se trouve dans vos contact.
  * android.permission.READ_CALL_LOG = Permet d'obtenir le numéro de téléphone d'un appelant
  * android.permission.RECORD_AUDIO = Permet d'utiliser le contrôle de bruit environnant
  * android.permission.QUERY_ALL_PACKAGES = Permet de lister les applications pour les notifications.


-------------------------
Dernière état de la compilation :  

[![pipeline status](https://gitlab.com/jnda/FlashAlert/badges/master/pipeline.svg)](https://gitlab.com/jnda/FlashAlert/-/commits/master)

Compilation :

  > ./gradlew assembleRelease

-------------------------

 L'application est disponible sur le magasin FDroid ( cliquez sur l'icône pour y accéder )

 [<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/fr.jnda.android.flashalert/)
