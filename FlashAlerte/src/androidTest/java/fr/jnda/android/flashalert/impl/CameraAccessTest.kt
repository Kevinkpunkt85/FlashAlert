package fr.jnda.android.flashalert.impl

import android.content.Context
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Before
import org.junit.Test

class CameraAccessTest {

    private var context: Context? = null

    @Before
    fun setUp() {
        context = InstrumentationRegistry.getInstrumentation().context
    }

    private val sms = arrayOf<Long>(150,10,150,10,150,10,30,5,30,5,30)
    private val call = arrayOf<Long>(80,5,80,5,80,5,30,5,30,5,30)
    private val default = arrayOf<Long>(50,30,50,30,50,30,50,30,50,30,50)

    @Test
    fun toggleStroboscopeDefault() {
        val cam = CameraAccess(context!!)
        cam.toggleStroboscope(default)
        val t = Thread {
        Thread.sleep(5000)
            cam.stopStroboscope()
        }
        t.start()
        t.join()
    }

    @Test
    fun toggleStroboscopeSMS() {
        val cam = CameraAccess(context!!)
        cam.toggleStroboscope(sms)
        val t = Thread {
            Thread.sleep(5000)
            cam.stopStroboscope()
        }
        t.start()
        t.join()
    }

    @Test
    fun toggleStroboscopeCall() {
        val cam = CameraAccess(context!!)
        cam.toggleStroboscope(call)
        val t = Thread {
            Thread.sleep(5000)
            cam.stopStroboscope()
        }
        t.start()
        t.join()
    }
}